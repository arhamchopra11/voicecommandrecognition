function varargout = Command(varargin)
% COMMAND MATLAB code for Command.fig
%      COMMAND, by itself, creates a new COMMAND or raises the existing
%      singleton*.
%
%      H = COMMAND returns the handle to a new COMMAND or the handle to
%      the existing singleton*.
%
%      COMMAND('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COMMAND.M with the given input arguments.
%
%      COMMAND('Property','Value',...) creates a new COMMAND or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Command_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Command_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Command

% Last Modified by GUIDE v2.5 27-Jan-2016 01:11:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Command_OpeningFcn, ...
                   'gui_OutputFcn',  @Command_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Command is made visible.
function Command_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Command (see VARARGIN)

% Choose default command line output for Command
handles.output = hObject;
handles.rec=audiorecorder();
handles.fs=8000;
handles.rasp=raspi('192.168.0.102');
handles.arduino=serialdev(handles.rasp,'/dev/ttyACM1',9600);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Command wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Command_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function process(hObject, eventdata, handles)
% hObject    handle to rec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.dat=Clip(NoiseRem(getaudiodata(handles.rec),8000),handles.fs);
sig=MFCCComp(handles.dat);
switch(sig)
   case 1
       set(handles.commands,'String','FORWARD','FontSize',18);
       write(handles.arduino,['f']);
   case 2
       set(handles.commands,'String','BACKWARD','FontSize',18);
       write(handles.arduino,['b']);
   case 3
       set(handles.commands,'String','RIGHT','FontSize',18);
       write(handles.arduino,['r']);
   case 4
       set(handles.commands,'String','LEFT','FontSize',18);
       write(handles.arduino,['l']);
   case 5
       set(handles.commands,'String','ROTATE','FontSize',18); 
       write(handles.arduino,['o']);
   case 6
       set(handles.commands,'String','TRACK','FontSize',18);  
       write(handles.arduino,['t']);
   case 7
       set(handles.commands,'String','END','FontSize',18)
       write(handles.arduino,['e']);
   case 8
       set(handles.commands,'String','STOP','FontSize',18);
       write(handles.arduino,['s']);
end
guidata(hObject, handles);


% --- Executes on button press in Stop.
function Stop_Callback(hObject, eventdata, handles)
% hObject    handle to Stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear(handles.rasp);
clear(handles.arduino)


% --- Executes on key press with focus on figure1 and none of its controls.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
if get(handles.figure1,'currentcharacter')=='r'
    record(handles.rec);
    set(handles.mb,'String','Recording','FontSize',20);
end


% --- Executes on key release with focus on figure1 and none of its controls.
function figure1_KeyReleaseFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was released, in lower case
%	Character: character interpretation of the key(s) that was released
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) released
% handles    structure with handles and user data (see GUIDATA)
if get(handles.figure1,'currentcharacter')=='r'
    stop(handles.rec);
    set(handles.mb,'String','Stop','FontSize',20);
    handles.dat=getaudiodata(handles.rec);
    guidata(hObject,handles);
    process(hObject, eventdata, handles);
end
function [yOut] = NoiseRemNClip(y,fs)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
t=0:1/fs:size(y,1)/fs-1/fs;
plot(t,y);
xlabel('Time'); ylabel('Amplitude');

N = size(y,1);
df = fs / N;
w = (-(N/2):(N/2)-1)*df;
y1 = fft(y(:,1), N) / N; %//For normalizing, but not needed for our analysis
y2 = fftshift(y1);
figure;
plot(w,abs(y2));

n = nFreq = 1000 / (fs/2);
endFreq = 4000 / (fs/2);
[b,a]7;
begi = butter(n, [beginFreq, endFreq], 'bandpass');

yOut = filter(b, a, y);
figure;
plot(t,yOut,'r');

y1 = fft(yOut(:,1), N) / N; %//For normalizing, but not needed for our analysis
y2 = fftshift(y1);
figure;
plot(w,abs(y2),'r');

i=1;
while yOut(i)<0.01
    i=i+1;
end
start=i;

i=size(yOut,1);
while yOut(i)<0.01
    i=i-1;
end
stop=i;
yOut=yOut(start:stop);
yOut=yOut/max(yOut);

figure;
plot(t(start:stop),yOut);
end
function varargout = Main(varargin)
% MAIN MATLAB code for Main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN.M with the given input arguments.
%
%      MAIN('Property','Value',...) creates a new MAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Main_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main

% Last Modified by GUIDE v2.5 27-Jan-2016 00:42:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main is made visible.
function Main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Main (see VARARGIN)

% Choose default command line output for Main
handles.output = hObject;
handles.dur=2;
handles.rec=audiorecorder();
handles.fs=8000;
handles.count=0;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Main wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in rec.
function rec_Callback(hObject, eventdata, handles)
% hObject    handle to rec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.count>=40
        set(handles.mb,'String','Click on Continue','FontSize',20);
else
set(handles.mb,'String','Start Speaking','FontSize',20);
count=floor((handles.count)/5)+1;
 switch(count)
    case 1
        set(handles.commands,'String','FORWARD','FontSize',18); 
    case 2
        set(handles.commands,'String','BACKWARD','FontSize',18);    
    case 3
        set(handles.commands,'String','RIGHT','FontSize',18);  
    case 4
        set(handles.commands,'String','LEFT','FontSize',18); 
    case 5
        set(handles.commands,'String','ROTATE','FontSize',18); 
    case 6
        set(handles.commands,'String','TRACK','FontSize',18);   
    case 7
        set(handles.commands,'String','END','FontSize',18); 
    case 8
        set(handles.commands,'String','STOP','FontSize',18);   
 end
recordblocking(handles.rec,handles.dur);
pause(handles.dur);
set(handles.mb,'String','Stop','FontSize',20);
 handles.dat=getaudiodata(handles.rec);
 handles.dat=NoiseRem(handles.dat,handles.fs);

end
guidata(hObject, handles);


% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.count=handles.count+1;
set(handles.mb,'String','Saving . . .','FontSize',20);
handles.dat=Clip(handles.dat,handles.fs);
i=mod(handles.count-1,5)+1;
switch(floor((handles.count-1)/5)+1)
   case 1
       name=sprintf('Forward%d.wav',i);
       audiowrite(name,handles.dat,8000);   
   case 2
       name=sprintf('Backward%d.wav',i);
       audiowrite(name,handles.dat,8000);
   case 3 
       name=sprintf('Right%d.wav',i);
       audiowrite(name,handles.dat,8000); 
   case 4 
       name=sprintf('Left%d.wav',i);
       audiowrite(name,handles.dat,8000);
   case 5
       name=sprintf('Rotate%d.wav',i);
       audiowrite(name,handles.dat,8000);   
   case 6 
       name=sprintf('Track%d.wav',i);
       audiowrite(name,handles.dat,8000);      
   case 7 
       name=sprintf('End%d.wav',i);
       audiowrite(name,handles.dat,8000); 
   case 8
       name=sprintf('Stop%d.wav',i);
       audiowrite(name,handles.dat,8000); 
end
pause(1);
set(handles.mb,'String','Saved','FontSize',20);
if handles.count>=27
    set(handles.mb,'String','Click on Continue','FontSize',20);
end
guidata(hObject, handles);



% --- Executes on button press in lstn.
function lstn_Callback(hObject, eventdata, handles)
% hObject    handle to lstn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.mb,'String','Playing','FontSize',20);
sound(handles.dat);
pause(handles.dur);
set(handles.mb,'String','End','FontSize',20);

% --- Executes on button press in cont.
function cont_Callback(hObject, eventdata, handles)
% hObject    handle to cont (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Command_handles=Command;
delete(get(hObject, 'parent')); % close GUI_1 


% --- Executes on key press with focus on figure1 and none of its controls.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
if get(handles.figure1,'currentcharacter')=='r'
     count=floor((handles.count)/3)+1;
 switch(count)
    case 1
        set(handles.commands,'String','FORWARD','FontSize',18); 
    case 2
        set(handles.commands,'String','BACKWARD','FontSize',18);    
    case 3
        set(handles.commands,'String','RIGHT','FontSize',18);  
    case 4
        set(handles.commands,'String','LEFT','FontSize',18); 
    case 5
        set(handles.commands,'String','ROTATE','FontSize',18); 
    case 6
        set(handles.commands,'String','TRACK','FontSize',18);   
    case 7
        set(handles.commands,'String','END','FontSize',18); 
    case 8
        set(handles.commands,'String','STOP','FontSize',18);   
 end
    record(handles.rec);
    set(handles.mb,'String','Recording','FontSize',20);
end


% --- Executes on key release with focus on figure1 and none of its controls.
function figure1_KeyReleaseFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was released, in lower case
%	Character: character interpretation of the key(s) that was released
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) released
% handles    structure with handles and user data (see GUIDATA)
if get(handles.figure1,'currentcharacter')=='r'
    stop(handles.rec);
    set(handles.mb,'String','Stop','FontSize',20);
    handles.dat=getaudiodata(handles.rec);
    handles.dat=NoiseRem(handles.dat,handles.fs);
end
guidata(hObject, handles);

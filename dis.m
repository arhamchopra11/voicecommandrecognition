function [d,endsig,enddat] = dis(sig,dat)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
endsig=size(sig,2);
enddat=size(dat,2);
for i=1:endsig
    for j=1:enddat
        d(i,j)=sqrt((sig(:,i)-dat(:,j))'*((sig(:,i)-dat(:,j))));
    end
end

end


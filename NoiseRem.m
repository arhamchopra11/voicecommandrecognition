function [ yOut ] = NoiseRem(y,fs)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
t=0:1/fs:size(y,1)/fs-1/fs;
% figure;
% plot(t,y);
% xlabel('Time'); ylabel('Amplitude');

N = size(y,1);
df = fs / N;
w = (-(N/2):(N/2)-1)*df;
y1 = fft(y(:,1), N) / N; 
y2 = fftshift(y1);
% figure;
% plot(w,abs(y2));

n = 10;
beginFreq = 800 / (fs/2);
endFreq = 3500 / (fs/2);
[b,a] = butter(n, [beginFreq, endFreq], 'bandpass');

yOut = filter(b, a, y);
% figure;
% plot(t,yOut,'r');

% y1 = fft(yOut(:,1), N) / N;
% y2 = fftshift(y1);
% figure;
% plot(w,abs(y2),'r');
end

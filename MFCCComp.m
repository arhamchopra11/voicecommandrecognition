function [index] = MFCCComp(sig)
%UNTITLED Summary of this function goes here
% y=Clip(y,8000);y=mfcc(y,8000,100); Detailed explanation goes here
sig=mfcc(sig,8000,100);

y=audioread('Forward1.wav');
y=Clip(y,8000);
%figure;plot(y); 
dat=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(dat));
q11=dtw(a,b,c);

y=audioread('Forward2.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q12=dtw(a,b,c);

y=audioread('Forward3.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q13=dtw(a,b,c);

y=audioread('Forward4.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q14=dtw(a,b,c);

y=audioread('Forward5.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q15=dtw(a,b,c);


y=audioread('Backward1.wav');
y=Clip(y,8000);
%figure;plot(y); 
dat=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(dat));
q21=dtw(a,b,c);

y=audioread('Backward2.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q22=dtw(a,b,c);

y=audioread('Backward3.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q23=dtw(a,b,c);

y=audioread('Backward4.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q24=dtw(a,b,c);

y=audioread('Backward5.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q25=dtw(a,b,c);


y=audioread('Right1.wav');
y=Clip(y,8000);
%figure;plot(y); 
dat=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(dat));
q31=dtw(a,b,c);

y=audioread('Right2.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q32=dtw(a,b,c);

y=audioread('Right3.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q33=dtw(a,b,c);

y=audioread('Right4.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q34=dtw(a,b,c);

y=audioread('Right5.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q35=dtw(a,b,c);


y=audioread('Left1.wav');
y=Clip(y,8000);
%figure;plot(y); 
dat=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(dat));
q41=dtw(a,b,c);

y=audioread('Left2.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q42=dtw(a,b,c);

y=audioread('Left3.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q43=dtw(a,b,c);

y=audioread('Left4.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q44=dtw(a,b,c);

y=audioread('Left4.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q45=dtw(a,b,c);

y=audioread('Rotate1.wav');
y=Clip(y,8000);
%figure;plot(y); 
dat=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(dat));
q51=dtw(a,b,c);

y=audioread('Rotate2.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q52=dtw(a,b,c);

y=audioread('Rotate3.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q53=dtw(a,b,c);

y=audioread('Rotate4.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q54=dtw(a,b,c);

y=audioread('Rotate5.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q55=dtw(a,b,c);


y=audioread('Track1.wav');
y=Clip(y,8000);
%figure;plot(y); 
dat=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(dat));
q61=dtw(a,b,c);

y=audioread('Track2.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q62=dtw(a,b,c);
 
y=audioread('Track3.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q63=dtw(a,b,c);

y=audioread('Track4.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q64=dtw(a,b,c);

y=audioread('Track5.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q65=dtw(a,b,c);


y=audioread('End1.wav');
y=Clip(y,8000);
%figure;plot(y); 
dat=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(dat));
q71=dtw(a,b,c);

y=audioread('End2.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q72=dtw(a,b,c);

y=audioread('End3.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q73=dtw(a,b,c);

y=audioread('End4.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q74=dtw(a,b,c);

y=audioread('End5.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q75=dtw(a,b,c);


y=audioread('Stop1.wav');
y=Clip(y,8000);
%figure;plot(y); 
dat=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(dat));
q81=dtw(a,b,c);

y=audioread('Stop2.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q82=dtw(a,b,c);

y=audioread('Stop3.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q83=dtw(a,b,c);

y=audioread('Stop4.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q84=dtw(a,b,c);

y=audioread('Stop5.wav');
y=Clip(y,8000);
%figure;plot(y); 
y=mfcc(y,8000,100);
[a,b,c]=dis(abs(sig),abs(y));
q85=dtw(a,b,c);

q1=[q11 q21 q31 q41 q51 q61 q71 q81];
[q1,index1]=min(q1);

q2=[q12 q22 q32 q42 q52 q62 q72 q82];
[q2,index2]=min(q2);

q4=[q14 q24 q34 q44 q54 q64 q74 q84];
[q4,index4]=min(q4);

q5=[q15 q25 q35 q45 q55 q65 q75 q85];
[q5,index5]=min(q5);

q3=[q13 q23 q33 q43 q53 q63 q73 q83];
[q3,index3]=min(q3);
    q=mode([q1 q2 q3 q4 q5]);
    switch(q)
   case q1
       index=index1;
   case q2
       index=index2;
   case q3
       index=index3;
   case q4
       index=index4;
   case q5
       index=index5;
   otherwise
        index=8;
end
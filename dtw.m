function [ val ] = dtw(d,endsig,enddat)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
mat(1,1)=d(1,1);
for i=2:enddat
    mat(1,i)=mat(1, i-1) + d(1,i);
end
for i=2:endsig
    mat(i,1)=mat(i-1, 1) + d(i,1);
end
for i=2:endsig
    for j=2:enddat
        mat(i,j)=min([mat(i-1, j-1), mat(i-1, j), mat(i, j-1)]) + d(i,j);
    end
end
val=mat(endsig,enddat);

end


function varargout = Command(varargin)
% COMMAND MATLAB code for Command.fig
%      COMMAND, by itself, creates a new COMMAND or raises the existing
%      singleton*.
%
%      H = COMMAND returns the handle to a new COMMAND or the handle to
%      the existing singleton*.
%
%      COMMAND('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COMMAND.M with the given input arguments.
%
%      COMMAND('Property','Value',...) creates a new COMMAND or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Command_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Command_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Command

% Last Modified by GUIDE v2.5 21-Jun-2015 17:18:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Command_OpeningFcn, ...
                   'gui_OutputFcn',  @Command_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Command is made visible.
function Command_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Command (see VARARGIN)

% Choose default command line output for Command
handles.output = hObject;
handles.rec=audiorecorder();
handles.fs=8000;

% handles.bt=serial('COM3','BaudRate',9600);
% fopen(handles.bt);

y=audioread('Forward1.wav');
handles.commands.f1=mfcc(y,8000,100);

y=audioread('Forward2.wav');
handles.commands.f2=mfcc(y,8000,100);

y=audioread('Forward3.wav');
handles.commands.f3=mfcc(y,8000,100);

y=audioread('Backward1.wav');
handles.commands.b1=mfcc(y,8000,100);

y=audioread('Backward2.wav');
handles.commands.b2=mfcc(y,8000,100);

y=audioread('Backward3.wav');
handles.commands.b3=mfcc(y,8000,100);

y=audioread('Right1.wav');
handles.commands.r1=mfcc(y,8000,100);

y=audioread('Right2.wav');
handles.commands.r2=mfcc(y,8000,100);

y=audioread('Right3.wav');
handles.commands.r3=mfcc(y,8000,100);

y=audioread('Left1.wav');
handles.commands.l1=mfcc(y,8000,100);

y=audioread('Left2.wav');
handles.commands.l2=mfcc(y,8000,100);

y=audioread('Left3.wav');
handles.commands.l3=mfcc(y,8000,100);

y=audioread('Stop1.wav');
handles.commands.s1=mfcc(y,8000,100);

y=audioread('Stop2.wav');
handles.commands.s2=mfcc(y,8000,100);

y=audioread('Stop3.wav');
handles.commands.s3=mfcc(y,8000,100);

y=audioread('Up1.wav');
handles.commands.u1=mfcc(y,8000,100);

y=audioread('Up2.wav');
handles.commands.u2=mfcc(y,8000,100);

y=audioread('Up3.wav');
handles.commands.u3=mfcc(y,8000,100);

y=audioread('Down1.wav');
handles.commands.d1=mfcc(y,8000,100);

y=audioread('Down2.wav');
handles.commands.d2=mfcc(y,8000,100);

y=audioread('Down3.wav');
handles.commands.d3=mfcc(y,8000,100);

y=audioread('Grip1.wav');
handles.commands.g1=mfcc(y,8000,100);

y=audioread('Grip2.wav');
handles.commands.g2=mfcc(y,8000,100);

y=audioread('Grip3.wav');
handles.commands.g3=mfcc(y,8000,100);

y=audioread('Free1.wav');
handles.commands.fr1=mfcc(y,8000,100);

y=audioread('Free2.wav');
handles.commands.fr2=mfcc(y,8000,100);

y=audioread('Free3.wav');
handles.commands.fr3=mfcc(y,8000,100);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Command wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Command_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in rec.
function rec_Callback(hObject, eventdata, handles)
% hObject    handle to rec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.mb,'String','Start Speaking','FontSize',20);
recordblocking(handles.rec,2);
pause(2);
set(handles.mb,'String','Stop','FontSize',20);
handles.dat=Clip(NoiseRem(getaudiodata(handles.rec),8000),handles.fs);
sig=MFCCComp2(handles.dat,handles.commands.f1,handles.commands.f2,handles.commands.f3,handles.commands.b1,handles.commands.b2,handles.commands.b3,handles.commands.r1,handles.commands.r2,handles.commands.r3,handles.commands.l1,handles.commands.l2,handles.commands.l3,handles.commands.s1,handles.commands.s2,handles.commands.s3,handles.commands.u1,handles.commands.u2,handles.commands.u3,handles.commands.d1,handles.commands.d2,handles.commands.d3,handles.commands.g1,handles.commands.g2,handles.commands.g3,handles.commands.fr1,handles.commands.fr2,handles.commands.fr3);
switch(sig)
   case 1
       set(handles.commands,'String','FORWARD','FontSize',18);
        %fprintf(handles.bt,'1');
   case 2
       set(handles.commands,'String','BACKWARD','FontSize',18);
        %fprintf(handles.bt,'2');
   case 3
       set(handles.commands,'String','RIGHT','FontSize',18);
        %fprintf(handles.bt,'3');
   case 4
       set(handles.commands,'String','LEFT','FontSize',18);
        %fprintf(handles.bt,'4');
   case 5
       set(handles.commands,'String','STOP','FontSize',18);
        %fprintf(handles.bt,'5');
   case 6
       set(handles.commands,'String','UP','FontSize',18);  
        %fprintf(handles.bt,'6');
   case 7
       set(handles.commands,'String','DOWN','FontSize',18);
        %fprintf(handles.bt,'7');
   case 8
       set(handles.commands,'String','GRIP','FontSize',18);
        %fprintf(handles.bt,'8');
   case 9
       set(handles.commands,'String','FREE','FontSize',18);
        %fprintf(handles.bt,'9');
%    case 10
%        set(handles.commands,'String','HOLD','FontSize',18);
end
guidata(hObject, handles);
   


% --- Executes on button press in Stop.
function Stop_Callback(hObject, eventdata, handles)
% hObject    handle to Stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fclose(handles.bt);

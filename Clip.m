function [yOut] = Clip(y,fs)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
t=0:1/fs:size(y,1)/fs-1/fs;
i=1;
y=y/max(abs(y));
ymax=max(y)*0.1;
while abs(y(i))<ymax
    i=i+1;
end
start=i;

i=size(y,1);
while abs(y(i))<ymax
    i=i-1;
end
stop=i;

yOut=y(start:stop);
yOut=yOut/max(abs(yOut));

% figure;
% plot(t(start:stop),yOut);

end
